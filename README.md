prerequisets: 
	- docker 
	- docker-compose

after installing the above docker and docker-compose you can run the below command to setup, migrate and host the application: "docker-compose up -d"

as well you can use the below command to view or to access the container:
	- docker ps : view active contaniners
	- docker exec -it {container name} bash : open a bash instance inside the container

after docker-compose command is done you can access the application on the following url: http://127.0.0.1:8010
