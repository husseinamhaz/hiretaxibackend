<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    protected $fillable = ["full_name", "mobile_phone", "arrival_date", "airport", "airflight_number", "terminal"];
}
