<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'full_name'=>'required',
            'mobile_phone'=>'required',
            'date_of_arrival'=>'required',
            'airport'=>'required',
            'airflight_number'=>'required|regex:/^[A-Z]{2,4}[1-9]{3,4}$/i'
        ];
    }
}
