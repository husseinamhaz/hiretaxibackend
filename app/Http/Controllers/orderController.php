<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\OrderRequest;
use App\Order;
use Mail;

class orderController extends Controller
{

    public function store(OrderRequest $request)
    {
        try {
            //get request data
            $data = $request->all();
            $query = new Order();
            if (isset($data['full_name']))
                $query->full_name = $data['full_name'];
            if (isset($data['mobile_phone']))
                $query->mobile_phone = $data['mobile_phone'];
            if (isset($data['date_of_arrival']))
                $query->date_of_arrival = $data['date_of_arrival'];
            if (isset($data['airport']))
                $query->airport = $data['airport'];
            if (isset($data['airflight_number']))
                $query->airflight_number = $data['airflight_number'];
            if (isset($data['terminal']))
                $query->terminal = $data['terminal'];
            //save order
            $query->save();
            //send email
            $this->mail($data);
            return $query;
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }

    public function mail()
    {

        try {
            //send email to husseinamhaz@hotmail.com
            Mail::send([], [], function ($message) {
                // Set the receiver and subject of the mail.
                $message->to('husseinamhaz@hotmail.com', 'Hussein Amhaz')->subject('New Taxi Order')
                    ->setBody('<h1>New Taxi Order!</h1> <div></div>', 'text/html');
                // Set the sender
                $message->from('ameerzahra112@gmail.com', 'New Taxi Order');
            });
            return "Basic email sent, check your inbox.";
        } catch (\Exception $e) {
            return [
                'message' => $e->getMessage(),
                'error' => $e->getCode(),
            ];
        }
    }
}
