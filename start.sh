chmod 777 -R storage/
chmod 777 -R public/
chmod 777 -R bootstrap/cache/
composer install
php artisan optimize
php artisan config:clear
php artisan migrate
composer dump-autoload
service apache2 restart
tail -f /var/log/apache2/midtier-error.log
